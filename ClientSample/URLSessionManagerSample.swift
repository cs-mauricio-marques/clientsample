//
//  URLSessionManagerSample.swift
//  ClientSample
//
//  Created by Douglas Barreto on 19/01/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation

enum ResponseError: Error {
	case jsonParserProblem
	case unknown
}

class URLSessionManagerSample {
	static let session = URLSession.shared
	
	static func getBooks(success : @escaping ([[String: Any]]) -> (), failure : @escaping (Error) -> () ) {
		let urlString = Enpoint.baseUrl + Enpoint.getBooks.value
		guard let url: URL = URL(string: urlString) else {
			print("[ERROR] Problem create a URL")
			return
		}
		
		//Create a Request to send to server
		var request = URLRequest(url: url)
		//Set the method type of request
		request.httpMethod = "GET"
		//Set to ignore all the cache stored on phone and every time the request be called, get a new one data
		request.cachePolicy = NSURLRequest.CachePolicy.reloadIgnoringCacheData
		
		//Open a new task to perform url request
		let task = session.dataTask(with: request as URLRequest) { (data, response, error) in
			guard let data = data, let _:URLResponse = response, error == nil else {
				print("[ERROR] Request problem \(error)")
				failure(error!)
				return
			}
			
			let jsonObj = try! JSONSerialization.jsonObject(with: data, options: .allowFragments)
			if let dict = jsonObj as? [[String: Any]] {
				print(dict)
				success(dict)
			} else {
				print("[ERROR] Problem to parse to dictionary")
				failure(ResponseError.jsonParserProblem)
			}
		}
		
		//Finish the task
		task.resume()
	}
	
}
