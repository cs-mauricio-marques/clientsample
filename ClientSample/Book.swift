//
//  Book.swift
//  ClientSample
//
//  Created by Mauricio Martinez Marques on 17/01/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation
import ObjectMapper

class Book : Mappable {
    
    var id : String?
    var author : String?
    var publisher : String?
    var title : String?
    
    init(id: String, author: String, publisher: String, title: String) {
        self.id = id
        self.author = author
        self.publisher = publisher
        self.title = title
    }
    
    //Mark: Mappable methods
    
    required init?(map: Map) {
        
    }
    
    func mapping(map: Map) {
        id <- map["id"]
        author <- map["author"]
        publisher <- map["publisher"]
        title <- map["title"]
    }
    
}
