//
//  AlamofireSampleManager.swift
//  ClientSample
//
//  Created by Mauricio Martinez Marques on 09/01/17.
//  Copyright © 2017 Concrete Solutions. All rights reserved.
//

import Foundation
import Alamofire

enum Enpoint {
	static let baseUrl = "http://localhost:3000"
	
    case getBooks
    case getBook(String)
    case newBook
    
    var value : String {
        switch self {
        case .getBooks:
            return "/getBooks"
        case .getBook(let id):
            return "/getBook/\(id)"
        case .newBook:
            return "/newBook"
        }
    }
}

//json book success block
typealias SuccessBlock = ([String:AnyObject]) -> ()
typealias FailureBlock = (Error) -> ()

class AlamofireSampleManager {
	
	let url = Enpoint.baseUrl
	
    func getBooks(success : @escaping ([[String:AnyObject]]) -> (), failure : @escaping (Error) -> ()) {
        
        let url = Enpoint.baseUrl
        let endpoint = Enpoint.getBooks.value
        
        Alamofire.request(url + endpoint, method: .get).responseJSON { (response) in
            
            if let error = response.result.error
            {
                failure(error)
            }
            else
            {
                debugPrint(response)
                
                if let json = response.result.value as? [[String : AnyObject]]
                {
                    success(json)
                }
            }
            
        }
    }
    
    func getBook(bookId : String, success : @escaping SuccessBlock, failure :  @escaping FailureBlock) {
        
        let url = Enpoint.baseUrl
        let endpoint = Enpoint.getBook(bookId).value
        
        Alamofire.request(url + endpoint, method: .get).response { (response) in
            
            if let error = response.error
            {
                failure(error)
            }
            else
            {
                debugPrint(response)
                
                do
                {
                    if let json = try JSONSerialization.jsonObject(with: response.data!, options: JSONSerialization.ReadingOptions.allowFragments) as? [[String : AnyObject]],
                       let book = json.first
                    {
                        success(book)
                    }
                    else
                    {
                        let error = NSError(domain: "error.jsonParse", code: 888, userInfo: nil)
                        failure(error)
                    }
                }
                catch let error
                {
                    failure(error)
                }

            }
            
        }
    }
    
    func newBook(book : Book, success : @escaping SuccessBookBlock, failure :  @escaping FailureBlock) {
    
        let url = Enpoint.baseUrl
        let endpoint = Enpoint.newBook.value
        
        let jsonBook = book.toJSON()
        
        Alamofire.request(url + endpoint, method: .post, parameters: jsonBook, encoding: JSONEncoding.default).responseJSON { (response) in
            
            if let error = response.result.error
            {
                failure(error)
            }
            else
            {
                debugPrint(response)
                
                if let json = response.result.value as? [[String : AnyObject]],
                   let newJSONBook = json.first
                {
                    if let newBook = Book(JSON: newJSONBook) {
                        success(newBook)
                    }else {
                        let error = NSError(domain: "error.incorrectJsonFormat", code: 889, userInfo: nil)
                        failure(error)
                    }
                }
            }
        }
        
    }
}

//Book class type success block
typealias SuccessBookBlock = (Book) -> ()
